#pragma once

#include <string>
#include <string_view>
#include <type_traits>

namespace cw::traits {
template<typename Type, typename... Arguments>
inline constexpr auto is_any_of_v{ (std::is_same_v<Type, Arguments> || ...) };

template<typename Type, typename Self>
using block_self_t = std::enable_if_t<!std::is_same_v<std::decay_t<Type>, std::decay_t<Self>>>;

template<typename Type>
inline constexpr auto is_c_string_v{ is_any_of_v<std::decay_t<Type>, const char *, char *> };

template<typename Type>
inline constexpr auto is_string_v{ is_any_of_v<std::decay_t<Type>, const char *, char *, std::string_view, std::string> };
}// namespace cw::traits
