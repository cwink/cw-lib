#pragma once

#include <cstdlib>

namespace cw {
constexpr auto exit_success{ EXIT_SUCCESS };

constexpr auto exit_failure{ EXIT_FAILURE };
}// namespace cw
