#pragma once

#include <iostream>
#include <sstream>

namespace cw::fmt::impl {
constexpr auto fprintf(std::ostream &os, std::string_view::const_iterator it, const std::string_view::const_iterator eit) -> void
{
  for (; it != eit; ++it) {
    os << *it;
  }
}

template<typename Type, typename... Arguments>
constexpr auto fprintf(std::ostream &os, std::string_view::const_iterator it, const std::string_view::const_iterator eit, const Type &value, const Arguments &... arguments) -> void
{
  for (; it != eit; ++it) {
    if (*it == '{' && ((it + 1) != eit && *(it + 1) == '}')) {
      os << value;

      it += 2;

      fprintf(os, it, eit, arguments...);

      return;
    }

    os << *it;
  }
}
}// namespace cw::fmt::impl

namespace cw::fmt {
template<typename... Arguments>
constexpr auto fprintf(std::ostream &os, const std::string_view &fmt, const Arguments &... arguments) -> void
{
  const auto *it{ std::cbegin(fmt) };

  const auto *eit{ std::cend(fmt) };

  impl::fprintf(os, it, eit, arguments...);
}

template<typename... Arguments>
constexpr auto printf(const std::string_view &fmt, const Arguments &... arguments) -> void
{
  fprintf(std::cout, fmt, arguments...);
}

template<typename... Arguments>
constexpr auto eprintf(const std::string_view &fmt, const Arguments &... arguments) -> void
{
  fprintf(std::cerr, fmt, arguments...);
}

template<typename... Arguments>
auto sprintf(const std::string_view &fmt, const Arguments &... arguments) -> std::string
{
  auto oss = std::ostringstream();

  fprintf(oss, fmt, arguments...);

  return oss.str();
}
}// namespace cw::fmt
