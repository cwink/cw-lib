#pragma once

#include "fmt.hpp"
#include "math.hpp"
#include "traits.hpp"

#include <algorithm>
#include <bits/exception.h>
#include <iostream>
#include <iterator>
#include <memory>
#include <sstream>
#include <string>
#include <string_view>
#include <type_traits>
#include <unordered_map>
#include <utility>
#include <variant>
#include <vector>

namespace cw::json {
enum class Type : std::uint8_t { null,
  number,
  boolean,
  string,
  array,
  object };

[[nodiscard]] constexpr auto string_from(const Type &type) noexcept -> std::string_view
{
  switch (type) {
  case Type::null:
    return "null";
  case Type::number:
    return "number";
  case Type::boolean:
    return "boolean";
  case Type::string:
    return "string";
  case Type::array:
    return "array";
  case Type::object:
    return "object";
  }
}

class Value;

using Null = std::monostate;

using Number = double;

using String = std::string;

using Boolean = bool;

using Array = std::vector<Value>;

namespace impl {
  using Object = std::unordered_map<std::string, Value>;
}// namespace impl

using Object = std::unique_ptr<impl::Object>;

using Data = std::variant<Null, Number, String, Boolean, Array, Object>;

template<typename TType>
[[nodiscard]] constexpr auto type_from() noexcept -> Type
{
  using T = std::decay_t<TType>;

  static_assert(traits::is_any_of_v<T, Null, Number, String, Boolean, Array, Object>, "expected null, number, string, boolean, array, or object type");

  if constexpr (std::is_same_v<T, Null>) {
    return Type::null;
  } else if constexpr (std::is_same_v<T, Number>) {
    return Type::number;
  } else if constexpr (std::is_same_v<T, Boolean>) {
    return Type::boolean;
  } else if constexpr (std::is_same_v<T, String>) {
    return Type::string;
  } else if constexpr (std::is_same_v<T, Array>) {
    return Type::array;
  } else if constexpr (std::is_same_v<T, Object>) {
    return Type::object;
  }
}

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wweak-vtables"
class Exception final : public std::exception
#pragma GCC diagnostic pop
{
public:
  explicit Exception(std::string message) : message_(std::move(message)) {}

  Exception(const Type expected, const Type given)
  {
    this->message_ = fmt::sprintf("invalid type, '{}' was expected, but '{}' was given", string_from(expected), string_from(given));
  }

  [[nodiscard]] auto what() const noexcept -> const char * override { return this->message_.c_str(); }

  Exception() = delete;

  Exception(const Exception &) = default;

  Exception(Exception &&) noexcept = default;

  auto operator=(const Exception &) -> Exception & = default;

  auto operator=(Exception &&) noexcept -> Exception & = default;

  ~Exception() noexcept override = default;

private:
  std::string message_;
};

class Value final
{
public:
  template<typename Type, typename = traits::block_self_t<Type, Value>>
  constexpr explicit Value(Type &&value)
  {
    this->set(std::forward<Type>(value));
  }

  template<typename Type, typename = traits::block_self_t<Type, Value>>
  constexpr auto operator=(Type &&value) -> Value &
  {
    this->set(std::forward<Type>(value));

    return *this;
  }

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wreturn-type"
  [[nodiscard]] constexpr auto type() const noexcept -> Type
  {
    if (std::holds_alternative<Null>(this->data_)) {
      return Type::null;
    }

    if (std::holds_alternative<Number>(this->data_)) {
      return Type::number;
    }

    if (std::holds_alternative<Boolean>(this->data_)) {
      return Type::boolean;
    }

    if (std::holds_alternative<String>(this->data_)) {
      return Type::string;
    }

    if (std::holds_alternative<Array>(this->data_)) {
      return Type::array;
    }

    if (std::holds_alternative<Object>(this->data_)) {
      return Type::object;
    }
  }
#pragma GCC diagnostic pop

  /*
  template<typename Type, typename = traits::block_self_t<Type, Value>>
  constexpr auto set(const Type &value) -> void
  {
    using T = std::decay_t<decltype(value)>;

    static_assert(traits::is_any_of_v<T, Null, Number, Boolean, Array, Object, impl::Object> || traits::is_string_v<T> || std::is_integral_v<T> || std::is_floating_point_v<T>,
      "value must be a null value, number, string, boolean, array, object, integer, or floating point type");

    if constexpr (std::is_same_v<T, std::string_view>) {
      this->data_ = String(value.data());
    } else if constexpr (traits::is_c_string_v<T>) {
      this->data_ = String(&value[0]);
    } else if constexpr (traits::is_any_of_v<T, Boolean, String, Array>) {
      this->data_ = value;
    } else if constexpr (traits::is_any_of_v<T, Number> || std::is_integral_v<T> || std::is_floating_point_v<T>) {
      this->data_ = static_cast<Number>(value);
    } else if constexpr (std::is_same_v<T, impl::Object>) {
      this->data_ = std::make_unique<impl::Object>(value);
    } else if constexpr (std::is_same_v<T, Object>) {
      this->data_ = std::make_unique<impl::Object>(*value);
    }
  }
  */

  template<typename Type, typename = traits::block_self_t<Type, Value>>
  constexpr auto set(Type &&value) -> void
  {
    using T = std::decay_t<decltype(value)>;

    static_assert(traits::is_any_of_v<T, Null, Number, Boolean, Array, Object, impl::Object> || traits::is_string_v<T> || std::is_integral_v<T> || std::is_floating_point_v<T>,
      "value must be a null value, number, string, boolean, array, object, integer, or floating point");

    if constexpr (std::is_same_v<T, std::string_view>) {
      this->data_ = String(value.data());
    } else if constexpr (traits::is_c_string_v<T>) {
      this->data_ = String(&value[0]);
    } else if constexpr (std::is_same_v<T, impl::Object>) {
      this->data_ = std::make_unique<impl::Object>(std::forward<T>(value));
    } else if constexpr (traits::is_any_of_v<T, Boolean, String, Array, Object>) {
      this->data_ = std::forward<T>(value);
    } else if constexpr (traits::is_any_of_v<T, Number> || std::is_integral_v<T> || std::is_floating_point_v<T>) {
      this->data_ = static_cast<Number>(value);
    }
  }

  template<typename Type>
  [[nodiscard]] constexpr auto get() -> std::conditional_t<std::is_same_v<std::decay_t<Type>, Object>, impl::Object, std::decay_t<Type>> &
  {
    using T = std::decay_t<Type>;

    static_assert(traits::is_any_of_v<T, Null, Number, String, Boolean, Array, Object>, "type must be null, number, string, boolean, array, or object");

    if (type_from<T>() != this->type()) {
      throw Exception(this->type(), type_from<T>());
    }

    if constexpr (std::is_same_v<T, Object>) {
      return *std::get<Object>(this->data_);
    } else {
      return std::get<T>(this->data_);
    }
  }

  template<typename Type>
  [[nodiscard]] constexpr auto get() const -> const std::conditional_t<std::is_same_v<std::decay_t<Type>, Object>, impl::Object, std::decay_t<Type>> &
  {
    using T = std::decay_t<Type>;

    static_assert(traits::is_any_of_v<T, Null, Number, String, Boolean, Array, Object>, "type must be null, number, string, boolean, array, or object");

    if (type_from<T>() != this->type()) {
      throw Exception(this->type(), type_from<T>());
    }

    if constexpr (std::is_same_v<T, Object>) {
      return *std::get<Object>(this->data_);
    } else {
      return std::get<T>(this->data_);
    }
  }

  template<typename Type>
  [[nodiscard]] constexpr auto begin() noexcept -> typename std::conditional_t<std::is_same_v<std::decay_t<Type>, Object>, impl::Object, std::decay_t<Type>>::iterator
  {
    return this->get<Type>().begin();
  }

  template<typename Type>
  [[nodiscard]] constexpr auto begin() const noexcept -> typename std::conditional_t<std::is_same_v<std::decay_t<Type>, Object>, impl::Object, std::decay_t<Type>>::const_iterator
  {
    return this->get<Type>().begin();
  }

  template<typename Type>
  [[nodiscard]] constexpr auto end() noexcept -> typename std::conditional_t<std::is_same_v<std::decay_t<Type>, Object>, impl::Object, std::decay_t<Type>>::iterator
  {
    return this->get<Type>().end();
  }

  template<typename Type>
  [[nodiscard]] constexpr auto end() const noexcept -> typename std::conditional_t<std::is_same_v<std::decay_t<Type>, Object>, impl::Object, std::decay_t<Type>>::const_iterator
  {
    return this->get<Type>().end();
  }

  template<typename Type>
  [[nodiscard]] constexpr auto cbegin() const noexcept -> typename std::conditional_t<std::is_same_v<std::decay_t<Type>, Object>, impl::Object, std::decay_t<Type>>::const_iterator
  {
    return this->get<Type>().cbegin();
  }

  template<typename Type>
  [[nodiscard]] constexpr auto cend() const noexcept -> typename std::conditional_t<std::is_same_v<std::decay_t<Type>, Object>, impl::Object, std::decay_t<Type>>::const_iterator
  {
    return this->get<Type>().cend();
  }

  template<typename Type>
  [[nodiscard]] constexpr auto at(const Type &lookup) -> Value &
  {
    using T = std::decay_t<Type>;

    static_assert(traits::is_string_v<T> || std::is_integral_v<T>, "lookup must be either an integral or a string");

    if constexpr (std::is_same_v<T, std::string>) {
      return this->get<Object>().at(lookup);
    } else if constexpr (std::is_same_v<T, std::string_view>) {
      return this->get<Object>().at(lookup.data());
    } else if constexpr (traits::is_c_string_v<T>) {
      return this->get<Object>().at(&lookup[0]);
    }

    return this->get<Array>().at(lookup);
  }

  template<typename Type>
  [[nodiscard]] constexpr auto at(const Type &lookup) const -> const Value &
  {
    using T = std::decay_t<Type>;

    static_assert(traits::is_string_v<T> || std::is_integral_v<T>, "lookup must be either an integral or a string");

    if constexpr (std::is_same_v<T, std::string>) {
      return this->get<Object>().at(lookup);
    } else if constexpr (std::is_same_v<T, std::string_view>) {
      return this->get<Object>().at(lookup.data());
    } else if constexpr (traits::is_c_string_v<T>) {
      return this->get<Object>().at(&lookup[0]);
    }

    return this->get<Array>().at(lookup);
  }

  template<class... Arguments>
  constexpr auto emplace_back(Arguments &&... arguments) -> Array::reference
  {
    return this->get<Array>().emplace_back(std::forward<Arguments>(arguments)...);
  }

  template<class... Arguments>
  constexpr auto emplace(Arguments &&... arguments) -> void
  {
    this->get<Object>().emplace(std::forward<Arguments>(arguments)...);
  }

  [[nodiscard]] constexpr auto empty() const -> bool
  {
    if (this->type() == Type::array) {
      return this->get<Array>().empty();
    }

    if (this->type() == Type::object) {
      return this->get<Object>().empty();
    }

    throw Exception("expected type to be an 'array' or an 'object'");
  }

  [[nodiscard]] constexpr auto contains(const std::string &key) const -> bool
  {
    if (this->type() == Type::object) {
      return this->get<Object>().find(key) != std::cend(this->get<Object>());
    }

    throw Exception("expected type to be an 'object'");
  }

  [[nodiscard]] constexpr auto size() const -> std::size_t
  {
    if (this->type() == Type::array) {
      return this->get<Array>().size();
    }

    if (this->type() == Type::object) {
      return this->get<Object>().size();
    }

    throw Exception("expected type to be an 'array' or 'object'");
  }

  [[nodiscard]] auto to_string() const -> std::string
  {
    switch (this->type()) {
    case Type::null:
      return "null";
    case Type::number:
      return std::to_string(this->get<Number>());
    case Type::boolean:
      return this->get<Boolean>() ? "true" : "false";
    case Type::string:
      return '"' + this->get<String>() + '"';
    case Type::array: {
      auto oss = std::ostringstream();

      oss << '[';

      if (!this->get<Array>().empty()) {
        auto it = this->cbegin<Array>();

        auto et = this->cend<Array>();

        oss << it->to_string();

        ++it;

        for (; it != et; ++it) {
          oss << ',' << it->to_string();
        }
      }

      oss << ']';

      return oss.str();
    }
    case Type::object: {
      auto oss = std::ostringstream();

      oss << '{';

      if (!this->get<Object>().empty()) {
        auto it = this->cbegin<Object>();

        auto et = this->cend<Object>();

        oss << '"' << it->first << "\":" << it->second.to_string();

        ++it;

        for (; it != et; ++it) {
          oss << ',' << "\"" << it->first << "\":" << it->second.to_string();
        }
      }

      oss << '}';

      return oss.str();
    }
    }
  }

  [[nodiscard]] auto to_string_pretty(const std::size_t indent_size = 2) const -> std::string
  {
    return this->to_string_pretty_(0, indent_size);
  }

  template<typename Type>
  constexpr auto operator[](Type &&lookup) -> Value &
  {
    using T = std::decay_t<Type>;

    static_assert(traits::is_string_v<T> || std::is_integral_v<T>, "lookup must be either an integral or a string");

    if constexpr (std::is_same_v<T, std::string_view>) {
      return this->get<Object>()[std::string(lookup.data())];
    } else if constexpr (traits::is_c_string_v<T>) {
      return this->get<Object>()[std::string(&lookup[0])];
    } else if constexpr (std::is_same_v<T, std::string>) {
      return this->get<Object>()[std::forward<T>(lookup)];
    } else {
      return this->get<Array>()[std::forward<T>(lookup)];
    }
  }

  template<typename Type>
  [[nodiscard]] constexpr auto operator[](const Type &lookup) const -> const Value &
  {
    using T = std::decay_t<Type>;

    static_assert(traits::is_string_v<T> || std::is_integral_v<T>, "lookup must be either an integral or a string");

    if constexpr (std::is_same_v<T, std::string_view>) {
      return this->get<Object>().at(lookup.data());
    } else if constexpr (traits::is_c_string_v<T>) {
      return this->get<Object>().at(&lookup[0]);
    } else if constexpr (std::is_same_v<T, std::string>) {
      return this->get<Object>().at(lookup);
    } else {
      return this->get<Array>().at(lookup);
    }
  }

  [[nodiscard]] constexpr explicit operator bool() const { return this->get<Boolean>(); }

  constexpr auto operator++() -> Value &
  {
    this->data_ = this->get<Number>() + 1.0;

    return *this;
  }

  auto operator++(int) -> Value
  {
    auto value = Value(this->get<Number>());

    this->operator++();

    return value;
  }

  constexpr auto operator--() -> Value &
  {
    this->data_ = this->get<Number>() - 1.0;

    return *this;
  }

  auto operator--(int) -> Value
  {
    auto value = Value(this->get<Number>());

    this->operator--();

    return value;
  }

  constexpr Value() = default;

  constexpr Value(const Value &value) { std::visit(Value::copy_visitor_(*this, value), value.data_); }

  Value(Value &&) noexcept = default;

  constexpr auto operator=(const Value &value) -> Value &
  {
    if (this == &value) {
      return *this;
    }

    std::visit(Value::copy_visitor_(*this, value), value.data_);

    return *this;
  }

  auto operator=(Value &&) noexcept -> Value & = default;

  ~Value() noexcept = default;

private:
  Data data_ = Null{};

  [[nodiscard]] auto to_string_pretty_(const std::size_t indent, const std::size_t indent_size) const -> std::string
  {
    switch (this->type()) {
    case Type::null:
      return "null";
    case Type::number:
      return std::to_string(this->get<Number>());
    case Type::boolean:
      return this->get<Boolean>() ? "true" : "false";
    case Type::string:
      return '"' + this->get<String>() + '"';
    case Type::array: {
      auto oss = std::ostringstream();

      oss << "[\n";

      if (!this->get<Array>().empty()) {
        auto it = this->cbegin<Array>();

        auto et = this->cend<Array>();

        oss << std::string(indent + indent_size, ' ') << it->to_string_pretty_(indent + indent_size, indent_size);

        ++it;

        for (; it != et; ++it) {
          oss << ",\n"
              << std::string(indent + indent_size, ' ')
              << it->to_string_pretty_(indent + indent_size, indent_size);
        }
      }

      oss << "\n"
          << std::string(indent, ' ') << "]";

      return oss.str();
    }
    case Type::object: {
      auto oss = std::ostringstream();

      oss << "{\n";

      if (!this->get<Object>().empty()) {
        auto it = this->cbegin<Object>();

        auto et = this->cend<Object>();

        oss << std::string(indent + indent_size, ' ') << '"' << it->first << "\" : " << it->second.to_string_pretty_(indent + indent_size, indent_size);

        ++it;

        for (; it != et; ++it) {
          oss << ",\n"
              << std::string(indent + indent_size, ' ') << "\"" << it->first << "\" : " << it->second.to_string_pretty_(indent + indent_size, indent_size);
        }
      }

      oss << "\n"
          << std::string(indent, ' ') << "}";

      return oss.str();
    }
    }
  }

  static auto constexpr copy_visitor_ = [](auto &first, const auto &second) {
    return [&first, &second](const auto &data) {
      using Type = std::decay_t<decltype(data)>;

      static_assert(traits::is_any_of_v<Type, Null, Number, String, Boolean, Array, Object>,
        "data value must be a null, number, string, boolean, array, or object type");

      if constexpr (traits::is_any_of_v<Type, Null, Number, String, Boolean, Array>) {
        first.data_ = data;
      } else if constexpr (std::is_same_v<Type, Object>) {
        first.data_ = std::make_unique<impl::Object>(*std::get<Object>(second.data_));
      }
    };
  };
};
}// namespace cw::json

namespace cw::json::build {
class Object final
{
public:
  template<class... Arguments>
  [[nodiscard]] constexpr auto add(Arguments &&... arguments) -> Object &
  {
    if (this->moved_) {
      throw Exception("can not add to object builder, internal object has already been moved");
    }

    this->object_.emplace(std::forward<Arguments>(arguments)...);

    return *this;
  }

  [[nodiscard]] auto build() -> Value
  {
    if (this->moved_) {
      throw Exception("can not build object, internal object has already been moved");
    }

    return Value(std::move(this->object_));
  }

  Object() = default;

  Object(const Object &) = delete;

  Object(Object &&) noexcept = default;

  auto operator=(const Object &) -> Object & = delete;

  auto operator=(Object &&) noexcept -> Object & = default;

  ~Object() noexcept = default;

private:
  impl::Object object_ = impl::Object();

  bool moved_{ false };
};

class Array final
{
public:
  template<class... Arguments>
  [[nodiscard]] constexpr auto add(Arguments &&... arguments) -> Array &
  {
    if (this->moved_) {
      throw Exception("can not add to array builder, internal array has already been moved");
    }

    this->array_.emplace_back(std::forward<Arguments>(arguments)...);

    return *this;
  }

  [[nodiscard]] auto build() -> Value
  {
    if (this->moved_) {
      throw Exception("can not add to array builder, internal array has already been moved");
    }

    return Value(std::move(this->array_));
  }

  Array() = default;

  Array(const Array &) = delete;

  Array(Array &&) noexcept = default;

  auto operator=(const Array &) -> Array & = delete;

  auto operator=(Array &&) noexcept -> Array & = default;

  ~Array() noexcept = default;

private:
  json::Array array_ = json::Array();

  bool moved_{ false };
};
}// namespace cw::json::build

inline auto operator<<(std::ostream &os, const cw::json::Value &value) -> std::ostream &
{
  os << value.to_string();

  return os;
}

// TODO(cwink) - Implement this.
//inline auto operator>>(std::istream &is, cw::json::Value &value) -> std::istream & {
//
//}

template<typename Type>
constexpr auto operator+=(cw::json::Value &first, const Type &second) -> cw::json::Value &
{
  using T = std::decay_t<Type>;

  static_assert(std::is_same_v<T, cw::json::Value> || std::is_floating_point_v<T> || std::is_integral_v<T>, "value type must be a JSON value, a floating point, or an integral");

  if constexpr (std::is_same_v<T, cw::json::Value>) {
    first.set(first.get<cw::json::Number>() + second.template get<cw::json::Number>());
  } else {
    first.set(first.get<cw::json::Number>() + static_cast<cw::json::Number>(second));
  }

  return first;
}

template<typename Type, typename = std::enable_if_t<std::is_floating_point_v<std::decay_t<Type>> || std::is_integral_v<std::decay_t<Type>>>>
constexpr auto operator+=(Type &first, const cw::json::Value &second) -> Type &
{
  first += static_cast<std::decay_t<Type>>(second.get<cw::json::Number>());

  return first;
}

template<typename Type>
constexpr auto operator-=(cw::json::Value &first, const Type &second) -> cw::json::Value &
{
  using T = std::decay_t<Type>;

  static_assert(std::is_same_v<T, cw::json::Value> || std::is_floating_point_v<T> || std::is_integral_v<T>, "value type must be a JSON value, a floating point, or an integral");

  if constexpr (std::is_same_v<T, cw::json::Value>) {
    first.set(first.get<cw::json::Number>() - second.template get<cw::json::Number>());
  } else {
    first.set(first.get<cw::json::Number>() - static_cast<cw::json::Number>(second));
  }

  return first;
}

template<typename Type, typename = std::enable_if_t<std::is_floating_point_v<std::decay_t<Type>> || std::is_integral_v<std::decay_t<Type>>>>
constexpr auto operator-=(Type &first, const cw::json::Value &second) -> Type &
{
  first -= static_cast<std::decay_t<Type>>(second.get<cw::json::Number>());

  return first;
}

template<typename Type>
constexpr auto operator*=(cw::json::Value &first, const Type &second) -> cw::json::Value &
{
  using T = std::decay_t<Type>;

  static_assert(std::is_same_v<T, cw::json::Value> || std::is_floating_point_v<T> || std::is_integral_v<T>, "value type must be a JSON value, a floating point, or an integral");

  if constexpr (std::is_same_v<T, cw::json::Value>) {
    first.set(first.get<cw::json::Number>() * second.template get<cw::json::Number>());
  } else {
    first.set(first.get<cw::json::Number>() * static_cast<cw::json::Number>(second));
  }

  return first;
}

template<typename Type, typename = std::enable_if_t<std::is_floating_point_v<std::decay_t<Type>> || std::is_integral_v<std::decay_t<Type>>>>
constexpr auto operator*=(Type &first, const cw::json::Value &second) -> Type &
{
  first *= static_cast<std::decay_t<Type>>(second.get<cw::json::Number>());

  return first;
}

template<typename Type>
constexpr auto operator/=(cw::json::Value &first, const Type &second) -> cw::json::Value &
{
  using T = std::decay_t<Type>;

  static_assert(std::is_same_v<T, cw::json::Value> || std::is_floating_point_v<T> || std::is_integral_v<T>, "value type must be a JSON value, a floating point, or an integral");

  if constexpr (std::is_same_v<T, cw::json::Value>) {
    if (second.template get<cw::json::Number>() == 0.0) {
      throw cw::json::Exception("can not divide by zero");
    }

    first.set(first.get<cw::json::Number>() / second.template get<cw::json::Number>());
  } else {
    if (static_cast<cw::json::Number>(second) == 0.0) {
      throw cw::json::Exception("can not divide by zero");
    }

    first.set(first.get<cw::json::Number>() / static_cast<cw::json::Number>(second));
  }

  return first;
}

template<typename Type, typename = std::enable_if_t<std::is_floating_point_v<std::decay_t<Type>> || std::is_integral_v<std::decay_t<Type>>>>
constexpr auto operator/=(Type &first, const cw::json::Value &second) -> Type &
{
  if (second.template get<cw::json::Number>() == 0.0) {
    throw cw::json::Exception("can not divide by zero");
  }

  first /= static_cast<std::decay_t<Type>>(second.get<cw::json::Number>());

  return first;
}

template<typename Type>
auto operator+(cw::json::Value first, const Type &second) -> cw::json::Value
{
  first += second;

  return first;
}

template<typename Type, typename = std::enable_if_t<std::is_floating_point_v<std::decay_t<Type>> || std::is_integral_v<std::decay_t<Type>>>>
constexpr auto operator+(Type first, const cw::json::Value &second) -> Type
{
  first += second;

  return first;
}

template<typename Type>
auto operator-(cw::json::Value first, const Type &second) -> cw::json::Value
{
  first -= second;

  return first;
}

template<typename Type, typename = std::enable_if_t<std::is_floating_point_v<std::decay_t<Type>> || std::is_integral_v<std::decay_t<Type>>>>
constexpr auto operator-(Type first, const cw::json::Value &second) -> Type
{
  first -= second;

  return first;
}

template<typename Type>
auto operator*(cw::json::Value first, const Type &second) -> cw::json::Value
{
  first *= second;

  return first;
}

template<typename Type, typename = std::enable_if_t<std::is_floating_point_v<std::decay_t<Type>> || std::is_integral_v<std::decay_t<Type>>>>
constexpr auto operator*(Type first, const cw::json::Value &second) -> Type
{
  first *= second;

  return first;
}

template<typename Type>
auto operator/(cw::json::Value first, const Type &second) -> cw::json::Value
{
  first /= second;

  return first;
}

template<typename Type, typename = std::enable_if_t<std::is_floating_point_v<std::decay_t<Type>> || std::is_integral_v<std::decay_t<Type>>>>
constexpr auto operator/(Type first, const cw::json::Value &second) -> Type
{
  first /= second;

  return first;
}

template<typename Type>
constexpr auto operator<(const cw::json::Value &first, const Type &second) -> bool
{
  using T = std::decay_t<Type>;

  static_assert(std::is_same_v<T, cw::json::Value> || std::is_floating_point_v<T> || std::is_integral_v<T>, "value type must be a JSON value, a floating point, or an integral");

  if constexpr (std::is_same_v<T, cw::json::Value>) {
    return first.get<cw::json::Number>() < second.template get<cw::json::Number>();
  } else {
    return first.get<cw::json::Number>() < static_cast<cw::json::Number>(second);
  }
}

template<typename Type>
constexpr auto operator<(const Type &first, const cw::json::Value &second) -> bool
{
  using T = std::decay_t<Type>;

  static_assert(std::is_same_v<T, cw::json::Value> || std::is_floating_point_v<T> || std::is_integral_v<T>, "value type must be a JSON value, a floating point, or an integral");

  if constexpr (std::is_same_v<T, cw::json::Value>) {
    return first.template get<cw::json::Number>() < second.get<cw::json::Number>();
  } else {
    return first < static_cast<T>(second.get<cw::json::Number>());
  }
}

template<typename Type>
constexpr auto operator>(const cw::json::Value &first, const Type &second) -> bool
{
  return second < first;
}

template<typename Type>
constexpr auto operator>(const Type &first, const cw::json::Value &second) -> bool
{
  return second < first;
}

template<typename Type>
constexpr auto operator<=(const cw::json::Value &first, const Type &second) -> bool
{
  return !(first > second);
}

template<typename Type>
constexpr auto operator<=(const Type &first, const cw::json::Value &second) -> bool
{
  return !(first > second);
}

template<typename Type>
constexpr auto operator>=(const cw::json::Value &first, const Type &second) -> bool
{
  return !(first < second);
}

template<typename Type>
constexpr auto operator>=(const Type &first, const cw::json::Value &second) -> bool
{
  return !(first < second);
}

constexpr auto operator==(const cw::json::Value &first, const cw::json::Value &second) -> bool
{
  if (first.type() != second.type()) {
    return false;
  }

  switch (first.type()) {
  case cw::json::Type::null:
    return true;
  case cw::json::Type::number:
    return cw::math::equals(first.get<cw::json::Number>(), second.get<cw::json::Number>());
  case cw::json::Type::string:
    return first.get<cw::json::String>() == second.get<cw::json::String>();
  case cw::json::Type::boolean:
    return first.get<cw::json::Boolean>() == second.get<cw::json::Boolean>();
  case cw::json::Type::array:
    if (first.size() != second.size()) {
      return false;
    }

    return std::equal(first.cbegin<cw::json::Array>(), first.cend<cw::json::Array>(), second.cbegin<cw::json::Array>(), [](const auto &left, const auto &right) { return left == right; });
  case cw::json::Type::object:
    if (first.size() != second.size()) {
      return false;
    }

    return std::all_of(first.cbegin<cw::json::Object>(), first.cend<cw::json::Object>(), [&second](const auto &pair) { return second.contains(pair.first) && second[pair.first] == pair.second; });
  }
}

template<typename Type, typename = cw::traits::block_self_t<Type, cw::json::Value>>
constexpr auto operator==(const Type &first, const cw::json::Value &second) -> bool
{
  using T = std::decay_t<Type>;

  static_assert(cw::traits::is_any_of_v<T, cw::json::Object, cw::json::impl::Object, cw::json::Array, cw::json::Null> || std::is_floating_point_v<T> || std::is_integral_v<T> || cw::traits::is_string_v<T>,
    "value type must be a JSON value, object, array, string, null, string view, c string, floating point, or an integral");

  if constexpr (std::is_same_v<T, cw::json::Object>) {
    return *first == second.get<cw::json::Object>();
  } else if constexpr (std::is_same_v<T, cw::json::impl::Object>) {
    return first == second.get<cw::json::Object>();
  } else if constexpr (std::is_same_v<T, cw::json::Array>) {
    return first == second.get<cw::json::Array>();
  } else if constexpr (cw::traits::is_string_v<T>) {
    return first == second.get<cw::json::String>();
  } else if constexpr (std::is_same_v<T, bool>) {
    return first == second.get<cw::json::Boolean>();
  } else if constexpr (std::is_floating_point_v<T> || std::is_integral_v<T>) {
    return cw::math::equals(first, static_cast<T>(second.get<cw::json::Number>()));
  }
}

template<typename Type, typename = cw::traits::block_self_t<Type, cw::json::Value>>
constexpr auto operator==(const cw::json::Value &first, const Type &second) -> bool
{
  return second == first;
}

constexpr auto operator!=(const cw::json::Value &first, const cw::json::Value &second) -> bool
{
  return !(first == second);
}

template<typename Type, typename = cw::traits::block_self_t<Type, cw::json::Value>>
constexpr auto operator!=(const Type &first, const cw::json::Value &second) -> bool
{
  return !(first == second);
}

template<typename Type, typename = cw::traits::block_self_t<Type, cw::json::Value>>
constexpr auto operator!=(const cw::json::Value &first, const Type &second) -> bool
{
  return !(first == second);
}
