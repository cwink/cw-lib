#pragma once

#include <cmath>
#include <type_traits>

namespace cw::math::impl {
constexpr auto epsilon{ 0.00001 };
}// namespace cw::math::impl

namespace cw::math {
template<typename Type>
constexpr auto equals(const Type first, const Type second) -> bool
{
  using T = std::decay_t<Type>;

  static_assert(std::is_arithmetic_v<T>, "first and second value must be a valid arithmetic type");

  if constexpr (std::is_integral_v<T>) {
    return first == second;
  } else {
    return std::fabs(static_cast<double>(first) - static_cast<double>(second)) < impl::epsilon;
  }
}
}// namespace cw::math
