add_executable(main.x main.cpp)

target_link_libraries(main.x PRIVATE base)

target_compile_features(main.x PRIVATE cxx_std_17)

set_project_compile_and_link_options(main.x)

add_custom_target(
  run
  COMMAND main.x
  DEPENDS main.x
  WORKING_DIRECTORY ${CMAKE_PROJECT_DIR})
