#include "../include/cw.hpp"
#include "../include/fmt.hpp"
#include "../include/json.hpp"
#include "../include/traits.hpp"

#include <bits/exception.h>
#include <fstream>
#include <iostream>

// TODO(cwink) - Create a new file class that handles open/read/write/etc. errors.
// on this thing, use "get<type>()" to get the value. Something like:
//   template<typename Type>
//   [[nodiscard]] constexpr auto get() -> Type {
//      Type value;
//
//      this->ifs >> value;
//
//      return value;
//   }
//
//    template<typename Type>
//   constexpr auto operator<<(std::istream &is, const Type &value) -> std::istream & {
//    ?

// TODO(cwink) -> To check for error, do "!ifs.eof() && ifs.fail()"

namespace cw::fs {
class Reader final
{
public:
  Reader() = delete;

  Reader(const Reader &) = delete;

  Reader(Reader &&) noexcept = default;

  auto operator=(const Reader &) -> Reader & = delete;

  auto operator=(Reader &&) noexcept -> Reader & = default;

  ~Reader() noexcept = default;

private:
  std::ifstream ifs_;
};
}// namespace cw::fs

auto main([[maybe_unused]] int argc, [[maybe_unused]] char *argv[]) -> int
{
  cw::fmt::fprintf(std::cout, "This is {} a {} test of {.\n", 123, 6.2);//NOLINT
  cw::fmt::printf(std::string_view("This is {} a {} test of {}.\n"), 123, 6.2);//NOLINT
  cw::fmt::eprintf(std::string("This is {} a {} test of {}.\n"), 123, 6.2);//NOLINT

  std::cout << cw::fmt::sprintf("This is {} a {} test of {}.", 123, 6.2) << '\n';//NOLINT

  try {
    auto j = cw::json::Object(new cw::json::impl::Object());// NOLINT

    (*j)["hahlolkkkk"] = "This is a tester";

    const auto z = cw::json::build::Object().add("hey", "how goes it?").add("some", 5.2).add("hehe", false).add("arr", cw::json::build::Array().add(false).add(1.2).add(-4).add(cw::json::build::Object().add("bringing", false).add("down", cw::json::Null{}).build()).build()).add("empty", cw::json::Null{}).add("another", cw::json::build::Object().add("kek", "lol").add("hehehe", 1.5).build()).build();

    const auto d = cw::json::build::Object().add("hey", "how goes it?").add("hehe", false).add("arr", cw::json::build::Array().add(false).add(1.2).add(-4).add(cw::json::build::Object().add("bringing", false).add("down", cw::json::Null{}).build()).build()).add("empty", cw::json::Null{}).add("another", cw::json::build::Object().add("kek", "lol").add("hehehe", 1.5).build()).build();

    auto v = cw::json::Value(5);//NOLINT

    auto k = cw::json::Value();

    k.set(j);

    std::cout << (*j)["hahlolkkkk"] << '\n';

    v += 2.0 + z["some"] + 1.0;//NOLINT

    std::cout << v << '\n';

    std::cout << (v == 13.2) << '\n';// NOLINT

    std::cout << (5.2 <= v) << '\n';// NOLINT

    std::cout << z << '\n';

    std::cout << z.to_string_pretty() << '\n';

    std::cout << (z == d) << '\n';
  } catch (std::exception &e) {
    std::cerr << e.what() << '\n';

    return cw::exit_failure;
  }

  return cw::exit_success;
}
