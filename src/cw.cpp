#include <iostream>

[[maybe_unused]] static auto foo() -> void
{
  std::cout << "foo" << '\n';
}
